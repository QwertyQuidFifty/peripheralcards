const CACHE_NAME = 'static-cache'
const urlsToCache = [
    '/index.html',
    '/index.js',
    '/style.css'
];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
        .then(function(cache) {
            console.log("Install success");
            return cache.addAll(urlsToCache);
        }).catch(function(error) {
        console.log('Install failed', error)
        })
    );
});

self.addEventListener('fetch',function(event) {
    console.log('Fetch event for ', event.request.url);
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            if (response) {
                console.log('Found ', event.request.url, ' in cache');
                return response                
            }
        console.log('Network request for ', event.request.url);
        return fetch(event.request)
        .then(function(response) {
            return caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log(event.request.url)
                cache.put(event.request.url, response.clone());
                return response;
            });
        });
        }).catch(function(error) {
        console.log('Fetch failed', error)
        })
    );
});
