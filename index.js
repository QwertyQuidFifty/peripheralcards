if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service-worker.js')
    .then(function(registration) {
        console.log('Registered', registration);
    }).catch(function(error) {
        console.log('Registration failed', error);
    });
}
var deckPathArray = 
[
    {
        "path": "/peripheralcards/decks/geography-world_flags.json",
        "rear": "Geography<hr>World Flags",
        "front": "<svg xmlns='http://www.w3.org/2000/svg' width='90%' height='60%' viewBox='0 0 3 2'><rect width='1' height='2' fill='#009246'/><rect width='1' height='2' x='1' fill='#fff'/><rect width='1' height='2' x='2' fill='#ce2b37'/></svg>"
    },
    {
        "path": "/peripheralcards/decks/chinese-2_character_verbs.json",
        "rear": "Mandarin<hr>2-Character Verbs<br>(HSK3-5)",
        "front": "起床",
        "front_init_font": "102px"
    },
    {
        "path": "/peripheralcards/decks/maths-basic_addition.json",
        "rear": "Maths<hr>Basic Addition",
        "front": "3 + 3 = ?<br>⚂ + ⚂"
    },
    {
        "path": "/peripheralcards/decks/japanese-basic_kanji1.json",
        "rear": "Japanese<hr>Basic Kanji 1",
        "front": "車<br>山        羊",
        "front_init_font": "120px",
        "rear_init_font": "56px"
    },
    {
        "path": "/peripheralcards/decks/korean_vocab-native_words.json",
        "rear": "Korean<hr>Native Vocab 1",
        "front": "나무<br>tree 🌳",
        "front_init_font": "96px",
        "rear_init_font": "56px"
    }
]

var playIcon = '<svg id="play" fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M8 5v14l11-7z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'
var pauseIcon = '<svg id="pause" fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z"/><path d="M0 0h24v24H0z" fill="none"/</svg>'
var nextIcon = '<svg id="next" fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z"/><path d="M0 0h24v24H0z" fill="none"/</svg>'
var openIcon = '<svg id="open" fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm0 12H4V8h16v10z"/></svg>'
var soundIcon = '<svg id="soundSVG" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M3 9v6h4l5 5V4L7 9H3zm13.5 3c0-1.77-1.02-3.29-2.5-4.03v8.05c1.48-.73 2.5-2.25 2.5-4.02zM14 3.23v2.06c2.89.86 5 3.54 5 6.71s-2.11 5.85-5 6.71v2.06c4.01-.91 7-4.49 7-8.77s-2.99-7.86-7-8.77z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

document.getElementById("play").innerHTML = playIcon;
document.getElementById("next").innerHTML = nextIcon;
document.getElementById("load").innerHTML = openIcon;
document.getElementById("check").insertAdjacentHTML("beforeend", soundIcon);


var itemTemplate = "<div id='front'><div id='frontCardText'>%front%</div></div><div id='rear'><div id='rearCardText'>%rear%</div></div>";

/*
var request = new XMLHttpRequest();
request.open("GET", deckPathArray[0].path, false);
request.send(null)
var data = JSON.parse(request.responseText); 
*/
var data = deckPathArray;
var parseData = {
  "front_init_font": "72px",
  "rear_init_font": "48px"
}
var speechSynthesis = speechSynthesis || webkitSpeechSynthesis;
//var voiceLang = deckPathArray[0].lang
var voiceLang
var voiceLang2
var randomNumber = 0;
var timeInterval = 8000;
var changeTimeout
var appearTimeout
var playlistLength = deckPathArray.length

var popupDiv = document.querySelector("#popup")


var popupDisplay = function(displayText) {
  popupDiv.innerText = displayText
  popupDiv.animate(
    [
        { opacity: 1 }, 
        { opacity: 0 }
    ], 3000);       
}
if("wakeLock" in navigator) {
  console.log("Wakelock")
  let wakeLock = null;
} else {
  popupDisplay("Wakelock API not available")       
}
async function wakeLockActivate() {
  try {
    wakeLock = await
    navigator.wakeLock.request("screen");
    popupDisplay("Wakelock active") 
  
  } catch (err) {
    popupDisplay(err)       
  }
}


var displayPlaylistItem = function(position) {
    var newItem = itemTemplate.replace("%front%",deckPathArray[position].front)
    newItem = newItem.replace("%rear%",deckPathArray[position].rear);   
    document.getElementById("mainScreen").innerHTML = newItem;
    document.getElementById("rear").style.display = "flex";
    adjustFontSize();
//    var deckDiv = document.getElementById("mainScreen").lastElementChild;
//    deckDiv.classList.add("deck")
}
var randomGenerate = function(data) {
    randomNumber = Math.floor(Math.random() * data.length) + 0;
    var newItem = itemTemplate.replace("%front%",data[randomNumber].front)
    newItem = newItem.replace("%rear%",data[randomNumber].rear);

    document.getElementById("mainScreen").innerHTML = newItem;
    adjustFontSize(); 
    if (document.getElementById("voiceOn").checked === true && voiceLang != "nil") {
        window.speechSynthesis.cancel();
        var voice = new SpeechSynthesisUtterance(data[randomNumber].front);
        voice.lang = voiceLang
        voice.rate = 0.8
        window.speechSynthesis.speak(voice);  
        
    }  
    appearTimeout = setTimeout(function() {
        document.getElementById("rear").style.display = "flex";
        adjustFontSize(); 
        if (document.getElementById("voiceOn").checked === true && voiceLang2 != "nil") {
//            window.speechSynthesis.cancel();
            console.log(data[randomNumber].rear.split("<")[0])
            var truncateText = data[randomNumber].rear.split("<")[0]
            var voice2 = new SpeechSynthesisUtterance(truncateText);
            voice2.lang = voiceLang2
            voice2.rate = 0.8
            window.speechSynthesis.speak(voice2);   
        }        
    }, timeInterval - 3000)
}
var itemCycle = function(data) {
  document.getElementById("play").innerHTML = pauseIcon;
  randomGenerate(data);
  changeTimeout = setInterval(function() {
      randomGenerate(data)
  }, timeInterval);
}

var playButton = document.getElementById("play");
playButton.addEventListener("click", function() {
  wakeLockActivate();
  if (data === deckPathArray) {
    fetch(data[randomNumber].path)
    .then(function(response) {
        return response.json();
    }).then(function(loadedJSON) {
        parseData = loadedJSON;
        data = loadedJSON.data
        voiceLang = loadedJSON.front_lang
        voiceLang2 = loadedJSON.rear_lang
        clearInterval(changeTimeout);
        clearInterval(appearTimeout);
        itemCycle(data); 
    }).catch(function(err) {
        popupDisplay(err)
        console.log(err)  
    })
  } else {
      if (this.firstChild.id === "pause") {
        clearInterval(appearTimeout);
        clearInterval(changeTimeout);
        document.getElementById("play").innerHTML = playIcon;
      } else {
        if (!document.getElementById("rear")) {
        itemCycle(data);    
      } else {
        document.getElementById("rear").style.display = "flex";
        document.getElementById("play").innerHTML = pauseIcon;
        setTimeout(function() {
          itemCycle(data);
        },1000);  
      }
    }
  }
});
var nextButton = document.getElementById("next");
nextButton.addEventListener("click", function() {
  if (data === deckPathArray) {
    randomNumber += 1;
    if (randomNumber > deckPathArray.length-1) {
      randomNumber = 0;
    }
    displayPlaylistItem(randomNumber);
  } else {      
    clearInterval(changeTimeout)
    clearInterval(appearTimeout)
    itemCycle(data);
  }
});
/*
var loadButton = document.getElementById("load");
loadButton.addEventListener("click", function() {
  var randomData = Math.floor(Math.random() * deckPathArray.length);
  console.log(randomData)
  voiceLang = deckPathArray[randomData].lang
//  fetch(deckPathArray[randomData].path)
//  .then(function(response) {
//    return response.json();
//  }).then(function(data) {
//    clearInterval(changeTimeout);
//    clearInterval(appearTimeout);
//    itemCycle(data);  
//  }).catch(function(err) {
//    console.log("Oops")  
//  })
    var request = new XMLHttpRequest();
    request.open("GET", deckPathArray[randomData].path, false);
    request.send(null)
    var data = JSON.parse(request.responseText); 
    clearInterval(changeTimeout);
    clearInterval(appearTimeout);
    itemCycle(data);    
//  if (randomData === 1) {
//    data = spanish;
//    voiceLang = "es"
//  } else if (randomData === 2) {
//    data = korean;
//    voiceLang = "ko"
//  }

});
*/
var loadButton = document.getElementById("load");
loadButton.addEventListener("click", function() {
  document.getElementById("fileSelect").click();
});

var voiceCheck = document.getElementById("voiceOn");
voiceCheck.addEventListener("change", function() {
  if (this.checked && voiceLang != "nil") {
    var voice = new SpeechSynthesisUtterance(data[randomNumber].front);
    voice.onstart = function(event) {
        console.log(event)
    }
    voice.lang = voiceLang;
    voice.rate = 0.8;
    window.speechSynthesis.speak(voice);
    const voiceList = window.speechSynthesis.getVoices();
    for (i in voiceList) {
        if (voiceList[i].lang === voice.lang) {
         popupDiv.innerText = "Loaded " + voiceList[i].name
        }
    }
    popupDiv.animate(
    [
        { opacity: 1 }, 
        { opacity: 0 }
    ], 5000);   
  } else {
    window.speechSynthesis.cancel();
  }
});
var soundButton = document.getElementById("check");
soundButton.addEventListener("click", function() {
  voiceCheck.click();
  if (voiceCheck.checked) {
    this.setAttribute("style", "opacity: 1.0")
    document.getElementById("soundSVG").setAttribute("fill", "#fff");
  } else {
    this.setAttribute("style", "opacity: 0.4")    
    document.getElementById("soundSVG").setAttribute("fill", "#000"); 
  }
});

var fileSelect = document.getElementById("fileSelect");
fileSelect.addEventListener("change", function() {
  var reader = new FileReader();
  reader.readAsText(fileSelect.files[0])
  reader.onload = function() {
    parseData = JSON.parse(reader.result)
    data = parseData.data
    voiceLang = parseData.front_lang
    voiceLang2 = parseData.rear_lang
    clearInterval(changeTimeout);
    clearInterval(appearTimeout);
    itemCycle(data);

  }
});


var adjustFontSize = function(string) {
//  var fontSize = (100 / string.length) - (string.length / 0.75);
//  var fontSize = 45 - (string.length*3);
//  if (fontSize <= 10) {
//      fontSize = 10;
//  }
  var container = document.getElementById("mainScreen");
  var frontText = document.getElementById("frontCardText");
  var rearText = document.getElementById("rearCardText");

  // Calculate the available width and height
  var containerWidth = container.offsetWidth;
  var containerHeight = container.offsetHeight/3;

  // Reset font size to default
  frontText.style.fontSize = "72px";
  rearText.style.fontSize = "72px";
  if(parseData.hasOwnProperty("front_init_font") === true) {
    frontText.style.fontSize = parseData.front_init_font;
  }
  if(parseData.hasOwnProperty("rear_init_font") === true) {
    rearText.style.fontSize = parseData.rear_init_font;
  }
  console.log(rearText.offsetHeight)
  console.log(containerHeight)



  // Reduce the font size until the text fits within the container

//  while (frontText.offsetWidth > containerWidth || frontText.offsetHeight > containerHeight) {
//    var fontSize = parseFloat(window.getComputedStyle(frontText).fontSize);
//    frontText.style.fontSize = (fontSize - 4) + "px";
//    console.log(fontSize)
//  }
//  frontText.style.fontSize = (fontSize - 2) + "px";

  while (rearText.offsetWidth > containerWidth || rearText.offsetHeight > containerHeight) {
    var fontSize = parseFloat(window.getComputedStyle(rearText).fontSize);
    rearText.style.fontSize = (fontSize - 4) + "px";
    console.log(fontSize)
  }
  rearText.style.fontSize = (fontSize - 2) + "px";
}

$(document).on("click", ".deck", function() {
    
})

$(document).on("swipeleft", ".deck", function() {
    console.log("Swipe left")
    if (randomNumber === 0) {
        randomNumber = playlistLength
    } else {
        randomNumber -= 1;
    }
    initiatePlaylist(randomNumber);
})

$(document).on("swiperight", ".deck", function() {
    console.log("Swipe right")
    if (randomNumber === playlistLength) {
        randomNumber = 0
    } else {
        randomNumber += 1;
    }
    initiatePlaylist(randomNumber);
})

randomGenerate(data);
document.getElementById("rear").style.display = "flex";
//initiatePlaylist(randomNumber)